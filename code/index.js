/* Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент
 массива этой функцией, возвращая новый массив. */

const num = [1, 4, 9];
const newNum = [];

function map(fn, newNum) {
    return newNum;
};

function fn() {
    for (let i = 0; i < num.length; i++) {
        newNum[i] = num[i] * 2;
    };
};

document.write("Исходный массив num: " + num.join(", ") + "<hr/>");
fn();
map();
document.write("Новый массив newNum, после умножения каждого элемента на 2: " + newNum.join(", ") + "<hr/>");

/* Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и
возвращает его результат.
1	function checkAge(age) {
2	if (age > 18) {
3	return true;
4	} else {
5	return confirm('Родители разрешили?');
6	} } */

let a = parseInt(prompt("Введите ваш возраст"));

function checkAge(age) {
    return a > 18 ? alert("true") : confirm("Родители разрешили?"); // или return (a > 18) || confirm("Родители разрешили?")
};

checkAge();